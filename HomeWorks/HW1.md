01.
Зарегистровать аккаунт на GitLab (https://gitlab.com/)
02.
Создать свой репозиторий с названием вашафамилия_python_beg
03.
Склонировать репозиторий себе на компьютер (git clone ссылка_на_ваш_репозиторий)
04.
Написать программу, которая принимает на вход какое то имя, и выводит 'Привет, имя_которое_ввели'
05. 
Добавить ее в репозиторий (git add Имя_вашего_файла.py)
06.
Закомитить (git commit -m "HW1")
07.
Запушить ее в репозиторий (git push origin master)
08.
Заполнить форму
https://docs.google.com/forms/d/1g5s-2Nr2htRoohTMq9-xAgIkdGmudRUwpKWbfcWe9Fo/viewform?edit_requested=true#responses
09. 
Мой репозиторий: https://gitlab.com/kondrartyom2/itpark_beginners_python/
10.
Плейлист: https://www.youtube.com/playlist?list=PLAbMP1vgx_nwABnnrYjh0Lf7k5Fc0t4cz
* Points 2
