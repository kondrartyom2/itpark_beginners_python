def count_of_upper_letter(string: str):
    answer = 0
    for word in string.split(' '):
        if 'A' <= word[0] <= 'Z' or 'А' <= word[0] <= 'Я':
            answer += 1
    return answer


if __name__ == '__main__':
    print(count_of_upper_letter(input('Введите строку - ')))