from dotenv import dotenv_values, find_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models.event import Event
from models.note import Note
from models.user import User

CONFIG = dotenv_values(find_dotenv(".env"))
conn_string = f'postgresql+psycopg2://{CONFIG.get("DB_USER_NAME")}:{CONFIG.get("DB_USER_PASS")}' \
              f'@{CONFIG.get("DB_HOST")}:{CONFIG.get("DB_PORT")}/{CONFIG.get("DB_NAME")}'
ENGINE = create_engine(conn_string, echo=False)
SESSION = sessionmaker(bind=ENGINE)


def add_user(user_id: int, name: str):
    session = SESSION()
    session.add(User(id=user_id, name=name))
    session.commit()


def add_note(title: str, text: str, user_id: int, timestamp):
    session = SESSION()
    user = get_user(user_id, session)
    session.add(Note(title=title, text=text, user_id=user.id, timestamp=timestamp))
    session.commit()


def add_event(title: str, text: str, user_id: int, timestamp):
    session = SESSION()
    user = get_user(user_id, session)
    session.add(Event(title=title, subscription=text, user_id=user.id, timestamp=timestamp))
    session.commit()


def soft_delete_user_by_id(user_id: int):
    session = SESSION()
    user = get_user(user_id, session=session)
    user.is_deleted = True
    session.add(user)
    session.commit()


def get_user(user_id: int, session=None) -> User:
    if session is None:
        session = SESSION()
    user = session.query(User).filter_by(id=user_id).first()
    return user


def soft_add_user_by_id(user_id: int):
    session = SESSION()
    user = get_user(user_id, session=session)
    user.is_deleted = False
    session.commit()


def get_notes_by_user(user_id: int):
    session = SESSION()
    notes = session.query(Note).filter_by(user_id=user_id)
    session.commit()
    return notes


def get_note_by_id(note_id: int):
    session = SESSION()
    note = session.query(Note).filter_by(id=note_id).first()
    session.close()
    return note


def get_event_by_id(note_id: int):
    session = SESSION()
    event = session.query(Event).filter_by(id=note_id).first()
    session.close()
    return event


def get_events_by_user(user_id: int):
    session = SESSION()
    events = session.query(Event).filter_by(user_id=user_id)
    session.commit()
    return events


def delete_note_by_id(note_id: int):
    session = SESSION()
    note = session.query(Note).filter_by(id=note_id).first()
    session.delete(note)
    session.commit()


def edit_note(note_id, title, text, timestamp):
    session = SESSION()
    note = session.query(Note).filter_by(id=note_id).first()
    note.title = title
    note.text = text
    note.timestamp = timestamp
    session.add(note)
    session.commit()


def delete_event_by_id(event_id):
    session = SESSION()
    event = session.query(Event).filter_by(id=event_id).first()
    session.delete(event)
    session.commit()