import os
import pathlib

DATE_REGEX = r'^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$'

ALLOWED_STRING_FORMATS=[
        '%d/%m/%y',
        '%d.%m.%y',
        '%d-%m-%y',
    ]

NOTE_TEMPLATE_PATH = os.environ.get(
    'NOTE_TEMPLATE_PATH',
    pathlib.Path().absolute().joinpath('static/note.html').absolute()
)

EVENT_TEMPLATE_PATH = os.environ.get(
    'EVENT_TEMPLATE_PATH',
    pathlib.Path().absolute().joinpath('static/event.html').absolute()
)

NOTE_EDIT_TEMPLATE_PATH = os.environ.get(
    'NOTE_EDIT_TEMPLATE_PATH',
    pathlib.Path().absolute().joinpath('static/edit_note.html').absolute()
)