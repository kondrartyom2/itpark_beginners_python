import datetime
import random
import re

from dotenv import dotenv_values, find_dotenv
from telebot import TeleBot, types

from service import add_user_if_not_exist, soft_delete_user_by_id, add_note, get_note_by_id, add_event, get_event_by_id, \
    delete_note_by_id, edit_note, delete_event_by_id
from settings import DATE_REGEX
from telegram_things.events_menu import get_all_events_menu, work_with_events_menu
from telegram_things.html_render import get_note_template, get_event_template, get_note_edit_template
from telegram_things.notes_menu import create_notes_menu, get_all_notes_menu, work_with_notes_menu
from telegram_things.telegram_helpers import get_user_name, hi_mess, convert_timestamp
from telegram_things.telegram_markups import start_button_markup, notes_button_markup, events_button_markup
from telegram_things.telegram_texts import *

CONFIG = dotenv_values(find_dotenv("../.env"))
TOKEN = CONFIG.get('TOKEN')
bot = TeleBot(TOKEN)

note_dic = {}

event_dic = {}


def random_sticker(message):
    """Ответ на не понятную команду"""
    bot.send_sticker(message.chat.id, STICKERS[random.randrange(0, 8)], reply_markup=start_button_markup())


def unclear_command(message):
    random_sticker(message)
    bot.send_message(message.chat.id, UNCLEAR_MESSAGE, parse_mode='html', reply_markup=start_button_markup())


def get_note_text(message):
    global note_dic
    note_dic[message.from_user.id]['text'] = message.text
    add_note(note_dic[message.from_user.id]['title'], note_dic[message.from_user.id]['text'], message.from_user.id)
    del note_dic[message.from_user.id]
    bot.send_message(message.chat.id, NOTE_ADD_SUCCESS, parse_mode='html', reply_markup=start_button_markup())


def get_note_text_edit(message):
    global note_dic
    note_dic[message.from_user.id]['text'] = message.text
    edit_note(int(note_dic[message.from_user.id]['id']), note_dic[message.from_user.id]['title'], note_dic[message.from_user.id]['text'])
    del note_dic[message.from_user.id]
    bot.send_message(message.chat.id, NOTE_ADD_SUCCESS, parse_mode='html', reply_markup=start_button_markup())


def get_note_title(message):
    global note_dic
    note_dic[message.from_user.id] = {'title': message.text}
    bot.send_message(message.chat.id, NOTE_TEXT, parse_mode='html')
    bot.register_next_step_handler(message, get_note_text)


def get_note_title_edit(call):
    global note_dic
    bot.send_message(call.chat.id, NOTE_TEXT, parse_mode='html')
    note_dic[call.message.from_user.id] = {'id': call.data.split(';')[1], 'title': call.message.text}

    bot.register_next_step_handler(call.message, get_note_text_edit)


def get_event_date(message):
    global event_dic
    user_date = message.text
    if re.match(DATE_REGEX, user_date) is not None:
        add_event(event_dic[message.from_user.id]['title'], event_dic[message.from_user.id]['subscription'],
                  convert_timestamp(user_date), message.from_user.id)
        del event_dic[message.from_user.id]
        bot.send_message(message.chat.id, EVENT_ADD_SUCCESS, parse_mode='html', reply_markup=start_button_markup())
    else:
        bot.send_message(message.chat.id, EVENT_DATE_INCORRECT, parse_mode='html')
        bot.register_next_step_handler(message, get_event_date)



def get_event_subscription(message):
    global event_dic
    event_dic[message.from_user.id]['subscription'] = message.text
    bot.send_message(message.chat.id, EVENT_DATE, parse_mode='html')
    bot.register_next_step_handler(message, get_event_date)


def get_event_title(message):
    global event_dic
    event_dic[message.from_user.id] = {'title': message.text}
    bot.send_message(message.chat.id, EVENT_SUBSCRIPTION, parse_mode='html')
    bot.register_next_step_handler(message, get_event_subscription)


def markup_edit_call(markup, message, call):
    bot.edit_message_text(message, call.from_user.id, call.message.message_id,
                          parse_mode='html', reply_markup=markup)


@bot.message_handler(commands=['start'])
def start(message):
    """Запуск бота"""
    name = get_user_name(message)
    add_user_if_not_exist(message.from_user.id, name)
    bot.send_message(message.chat.id, hi_mess(name), parse_mode='html')
    bot.send_message(message.chat.id, COMMAND_PHRASE, parse_mode='html', reply_markup=start_button_markup())


@bot.message_handler(commands=['off'])
def off(message):
    """Остановка бота"""
    soft_delete_user_by_id(message.from_user.id)


@bot.message_handler(commands=['info'])
def info(message):
    """Вывод списка имеющихся команд"""
    bot.send_message(message.chat.id, BOT_COMMANDS_MESSAGE, parse_mode='html', reply_markup=start_button_markup())


@bot.message_handler(commands=['notes'])
def notes_work(message):
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    bot.send_message(message.chat.id, NOTES_WORK, parse_mode='html',
                     reply_markup=notes_button_markup())


@bot.message_handler(commands=['notes-add'])
def notes_add(message):
    add_user_if_not_exist(message.from_user.id, get_user_name(message))

    bot.send_message(message.chat.id, NOTE_TITLE,
                     parse_mode='html', reply_markup=types.ReplyKeyboardRemove())

    bot.register_next_step_handler(message, get_note_title)


@bot.message_handler(commands=['notes-view'])
def notes_view(message):
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    markup = get_all_notes_menu(message.from_user.id)
    bot.send_message(message.chat.id, NOTES, parse_mode='html',
                     reply_markup=markup)


@bot.message_handler(commands=['events'])
def events_work(message):
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    bot.send_message(message.chat.id, EVENTS_WORK, parse_mode='html',
                     reply_markup=events_button_markup())


@bot.message_handler(commands=['events-add'])
def events_add(message):
    add_user_if_not_exist(message.from_user.id, get_user_name(message))

    bot.send_message(message.chat.id, EVENT_TITLE,
                     parse_mode='html', reply_markup=types.ReplyKeyboardRemove())

    bot.register_next_step_handler(message, get_event_title)


@bot.message_handler(commands=['events-view'])
def event_view(message):
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    markup = get_all_events_menu(message.from_user.id)
    bot.send_message(message.chat.id, EVENTS, parse_mode='html',
                     reply_markup=markup)


@bot.callback_query_handler(func=lambda call: 'NOTE' == call.data.split(';')[0])
def handle_note_query(call):
    add_user_if_not_exist(call.from_user.id, get_user_name(call))

    note_id = call.data.split(';')[1]

    markup_edit_call(work_with_notes_menu(note_id),
                     get_note_template(get_note_by_id(int(note_id))), call)


@bot.callback_query_handler(func=lambda call: 'BACK-NOTE' == call.data.split(';')[0])
def handle_note_back(call):
    add_user_if_not_exist(call.from_user.id, get_user_name(call))

    markup_edit_call(get_all_notes_menu(call.from_user.id), NOTES, call)


@bot.callback_query_handler(func=lambda call: 'DELETE-NOTE' == call.data.split(';')[0])
def handle_note_delete(call):
    add_user_if_not_exist(call.from_user.id, get_user_name(call))

    delete_note_by_id(call.data.split(';')[1])

    bot.answer_callback_query(call.id, text="Заметка удалена")

    markup_edit_call(get_all_notes_menu(call.from_user.id), NOTES, call)


@bot.callback_query_handler(func=lambda call: 'EDIT-NOTE' == call.data.split(';')[0])
def handle_note_edit(call):
    add_user_if_not_exist(call.from_user.id, get_user_name(call))

    markup_edit_call(None, get_note_edit_template(get_note_by_id(call.data.split(';')[1])), call)

    delete_note_by_id(call.data.split(';')[1])

    notes_add(call.message)


@bot.callback_query_handler(func=lambda call: 'EVENT' == call.data.split(';')[0])
def handle_note_query(call):
    add_user_if_not_exist(call.from_user.id, get_user_name(call))

    event_id = call.data.split(';')[1]

    markup_edit_call(work_with_events_menu(event_id),
                     get_event_template(get_event_by_id(int(event_id))), call)


@bot.callback_query_handler(func=lambda call: 'BACK-EVENT' == call.data.split(';')[0])
def handle_note_back(call):
    add_user_if_not_exist(call.from_user.id, get_user_name(call))

    markup_edit_call(get_all_events_menu(call.from_user.id), EVENTS, call)


@bot.callback_query_handler(func=lambda call: 'COMPLETE-EVENT' == call.data.split(';')[0])
def handle_note_delete(call):
    add_user_if_not_exist(call.from_user.id, get_user_name(call))

    delete_event_by_id(call.data.split(';')[1])

    bot.answer_callback_query(call.id, text="Мероприятие прошло")

    markup_edit_call(get_all_events_menu(call.from_user.id), EVENTS, call)


@bot.message_handler(content_types=['text'])
def keyboard_buttons_response(message):
    """Обработка клавиатурных кнопок"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    get_message_text = message.text.strip().lower()
    if get_message_text == 'notes':
        notes_work(message)
    elif get_message_text == 'add note':
        notes_add(message)
    elif get_message_text == 'view notes':
        notes_view(message)
    elif get_message_text == 'events':
        events_work(message)
    elif get_message_text == 'add event':
        events_add(message)
    elif get_message_text == 'view events':
        event_view(message)
    elif get_message_text == 'info':
        bot.send_message(message.chat.id, "Выход", reply_markup=start_button_markup())
    elif get_message_text == 'setup':
        bot.answer_callback_query(message.chat.id, text="Настройки")
    elif get_message_text == 'exit':
        bot.send_message(message.chat.id, "Выход", reply_markup=start_button_markup())
    else:
        unclear_command(message)


bot.polling(none_stop=True)