import datetime

import db


def check_user(user_id: int):
    user = db.get_user(user_id)
    if user is not None:
        return user
    return None


def add_user(user_id: int, name: str):
    db.add_user(user_id, name)


def add_user_if_not_exist(user_id: int, name: str):
    checked_user = check_user(user_id)
    if checked_user is None:
        add_user(user_id, name)
    else:
        if checked_user.is_deleted:
            checked_user.is_deleted = False
            db.soft_add_user_by_id(checked_user.id)


def add_note(title: str, text: str, user_id: int):
    db.add_note(title, text, user_id, datetime.datetime.now())


def add_event(title: str, subscription: str, date, user_id: int):
    db.add_event(title, subscription, user_id, date)


def soft_delete_user_by_id(user_id: int):
    db.soft_delete_user_by_id(user_id)


def get_notes_by_user(user_id: int):
    return db.get_notes_by_user(user_id)


def get_note_by_id(note_id: int):
    return db.get_note_by_id(note_id)


def get_events_by_user(user_id: int):
    return db.get_events_by_user(user_id)


def delete_note_by_id(note_id: int):
    db.delete_note_by_id(note_id)


def delete_event_by_id(event_id: int):
    db.delete_event_by_id(event_id)


def get_event_by_id(event_id: int):
    return db.get_event_by_id(event_id)


def edit_note(note_id: int, title: str, text: str):
    db.edit_note(note_id, title, text, datetime.datetime.now())