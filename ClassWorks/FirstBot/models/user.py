from sqlalchemy import Column, BigInteger, VARCHAR, BOOLEAN

from models.base import Base


class User(Base):
    __tablename__ = 'user_setup'

    id = Column(BigInteger, primary_key=True)
    name = Column(VARCHAR)
    is_deleted = Column(BOOLEAN, nullable=False, default=False)

    def __repr__(self):
        return "<User(id=%s, name=%s, is_deleted=%s)>" % \
               (self.id, self.name, self.is_deleted)
