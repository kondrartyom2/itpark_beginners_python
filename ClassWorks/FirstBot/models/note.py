from sqlalchemy import Column, VARCHAR, BigInteger, ForeignKey, TIMESTAMP

from models.base import Base


class Note(Base):
    __tablename__ = 'note'

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    title = Column(VARCHAR)
    text = Column(VARCHAR)
    timestamp = Column(TIMESTAMP)
    user_id = Column(BigInteger, ForeignKey('user_setup.id'))

    def __repr__(self):
        return "<Note(id=%s, title=%s, text=%s, user_id=%s, timestamp=%s)>" % \
               (self.id, self.title, self.text, self.user_id, self.timestamp)
