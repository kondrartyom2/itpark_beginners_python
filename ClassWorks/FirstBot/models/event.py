from sqlalchemy import Column, BigInteger, VARCHAR, TIMESTAMP, ForeignKey

from models.base import Base


class Event(Base):
    __tablename__ = 'event'

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    title = Column(VARCHAR)
    subscription = Column(VARCHAR)
    timestamp = Column(TIMESTAMP)
    user_id = Column(BigInteger, ForeignKey('user_setup.id'))

    def __repr__(self):
        return "<Event(id=%s, title=%s, subscription=%s, user_id=%s, timestamp=%s)>" % \
               (self.id, self.title, self.text, self.user_id, self.timestamp)
