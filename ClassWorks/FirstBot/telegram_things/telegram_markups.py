from telebot import types


"""Тут мы занимаемся кнопками на клавиатуре, которые в свою очередь отправляют просто сообщение"""


def start_button_markup():
    """Создания кнопок на клавиатуре"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    btn1 = types.KeyboardButton('Notes')
    btn2 = types.KeyboardButton('Events')
    btn3 = types.KeyboardButton('Setup')
    btn4 = types.KeyboardButton('Info')
    markup.row(btn1, btn2)
    markup.row(btn3, btn4)
    return markup


def notes_button_markup():
    """Кнопки для работы с заметками"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    add = types.KeyboardButton('Add note')
    view = types.KeyboardButton('View notes')
    exit = types.KeyboardButton('Exit')
    markup.row(add, view)
    markup.row(exit)
    return markup


def events_button_markup():
    """Кнопки для работы с мероприятиями"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    add = types.KeyboardButton('Add event')
    view = types.KeyboardButton('View events')
    exit = types.KeyboardButton('Exit')
    markup.row(add, view)
    markup.row(exit)
    return markup