import json

from db import get_events_by_user

"""Тут мы создаем меню с кнопками не на клавиатуре"""


def create_callback_data_menus(action, choose):
    return ";".join([action, choose])


def get_all_events_menu(user_id: int):
    markup = {"inline_keyboard": []}

    events = get_events_by_user(user_id)

    for event in events:
        row = [{"text": event.title, "callback_data": create_callback_data_menus("EVENT", str(event.id))}]
        markup["inline_keyboard"].append(row)

    return json.dumps(markup)


def create_events_menu():
    markup = {"inline_keyboard": []}

    row = [{"text": 'Добавить', "callback_data": create_callback_data_menus("ADD-EVENT", "event")}]
    markup["inline_keyboard"].append(row)

    row = [{"text": 'Посмотреть', "callback_data": create_callback_data_menus("VIEW-EVENT", "event")}]
    markup["inline_keyboard"].append(row)

    return json.dumps(markup)


def work_with_events_menu(event_id: int):
    markup = {"inline_keyboard": []}

    row = [{"text": "✅", "callback_data": create_callback_data_menus("COMPLETE-EVENT", str(event_id))}]
    markup["inline_keyboard"].append(row)

    row = [{"text": "Назад", "callback_data": create_callback_data_menus("BACK-EVENT", str(event_id))}]
    markup["inline_keyboard"].append(row)

    return json.dumps(markup)