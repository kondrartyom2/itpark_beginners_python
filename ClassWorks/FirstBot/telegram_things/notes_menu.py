import json

from service import get_notes_by_user


def create_callback_data_menus(action, choose):
    return ";".join([action, choose])


def create_notes_menu():
    markup = {"inline_keyboard": []}

    row = [{"text": 'Добавить', "callback_data": create_callback_data_menus("ADD-NOTE", "note")}]
    markup["inline_keyboard"].append(row)

    row = [{"text": 'Посмотреть', "callback_data": create_callback_data_menus("VIEW-NOTES", "note")}]
    markup["inline_keyboard"].append(row)

    return json.dumps(markup)


def get_all_notes_menu(user_id: int):
    markup = {"inline_keyboard": []}

    notes = get_notes_by_user(user_id)

    for note in notes:
        row = [{"text": note.title, "callback_data": create_callback_data_menus("NOTE", str(note.id))}]
        markup["inline_keyboard"].append(row)

    return json.dumps(markup)


def work_with_notes_menu(note_id: int):
    markup = {"inline_keyboard": []}


    row = [{"text": "Удалить", "callback_data": create_callback_data_menus("DELETE-NOTE", str(note_id))}]
    markup["inline_keyboard"].append(row)

    row = [{"text": "Редактировать", "callback_data": create_callback_data_menus("EDIT-NOTE", str(note_id))}]
    markup["inline_keyboard"].append(row)

    row = [{"text": "Назад", "callback_data": create_callback_data_menus("BACK-NOTE", str(note_id))}]
    markup["inline_keyboard"].append(row)

    return json.dumps(markup)