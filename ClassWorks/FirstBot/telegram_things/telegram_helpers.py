from datetime import datetime

from settings import ALLOWED_STRING_FORMATS
from telegram_things.telegram_texts import WELCOME_MESSAGE


def hi_mess(name: str):
    """Вывод приветственного уведомления"""
    return "Привет, " + name + " !" + WELCOME_MESSAGE


def get_user_name(message):
    if (message.from_user.first_name is not None) & (message.from_user.last_name is not None):
        name = message.from_user.first_name

    elif (message.from_user.first_name is not None) & (message.from_user.last_name is None):
        name = message.from_user.first_name

    elif message.from_user.username is not None:
        name = message.from_user.username

    else:
        name = 'Пользователь'

    return name


def convert_timestamp(date_timestamp, output_format='%d-%m-%y'):
    for regex in ALLOWED_STRING_FORMATS:
        try:
            date = datetime.strptime(date_timestamp, regex)
            return date.strftime(output_format)
        except ValueError:
            pass
    return datetime.now().strftime(output_format)