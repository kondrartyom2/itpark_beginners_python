from jinja2 import Template

from settings import NOTE_TEMPLATE_PATH, EVENT_TEMPLATE_PATH, NOTE_EDIT_TEMPLATE_PATH


"""Тут мы занимаемся подготовкой наших html шаблонов(подставляем в них данные) для отправки их пользователю"""


def get_note_template(note):
    template = Template(open(NOTE_TEMPLATE_PATH, encoding='utf-8').read())
    return template.render(note=note)


def get_note_edit_template(note):
    template = Template(open(NOTE_EDIT_TEMPLATE_PATH, encoding='utf-8').read())
    return template.render(note=note)


def get_event_template(event):
    template = Template(open(EVENT_TEMPLATE_PATH, encoding='utf-8').read())
    return template.render(event=event)