WELCOME_MESSAGE = "\nЯ бот для заметок, давай познакомимся поближе!"\
                  "\nЧтобы получить список моих возможностей введите команду - " + "/info" + \
                  "\nДля помощи и исправления багов обращайтесь к @kondrartyom"

#language=HTML
BOT_COMMANDS_MESSAGE = '<b>У меня есть следующие команды:</b>' \
                       '\n<b>/start - </b>начать все сначала.\n' \
                       '\n<b>/off - </b>остановить бота.\n'

STICKERS = {
    0: 'CAACAgIAAxkBAAMoXpzP3CWCkQxsVDLY5hEUGBGOuc0AAvgAAwkSNAABLUsVuOP-dWQYBA',
    1: 'CAACAgIAAxkBAAMuXpzRGeswmw_N25IHrPx8GGGVddAAAmMDAAJS-REHdXjf1JTAZtYYBA',
    2: 'CAACAgIAAxkBAAM0XpzS1WiBaKvY7EhHRb_1dGt9w6sAAqUFAAJTsfcDr2zCCbbAFqYYBA',
    3: 'CAACAgIAAxkBAAM4XpzTIJ1-QEUEhUEga0IsvJ1uQsQAAhUAA3Xtxh-EZKYCEPTsYxgE',
    4: 'CAACAgIAAxkBAAK_NF69x5obwRkQ6seLq4FOKXnXnCEIAAISAAO0D9kVYGWn6sMwOp4ZBA',
    5: 'CAACAgIAAxkBAAM6XpzTKYWGVJW1SUqFEGj_uCEOCs4AAgEAA3Xtxh-GBXqzY9NeWhgE',
    6: 'CAACAgIAAxkBAAK_LF69x2i7IW-JgfyxAicr2TTMDGH7AAIBAAN17cYfhgV6s2PTXloZBA',
    7: 'CAACAgIAAxkBAAK_Ll69x3NtshYFo3cF0CrEBUDq1Ub1AAIPAAN17cYf_Fe6qaMBp1QZBA',
    8: 'CAACAgIAAxkBAAK_Ql69yFHRjJSk-D4nH_9DdRrRC6FdAAJcAANFqmQMQ3oxkFr5F8AZBA'
}

#language=HTML
UNCLEAR_MESSAGE = '<b>Давай еще раз ;)</b>\nЧто я должен сделать? '

#language=HTML
COMMAND_PHRASE = "\nЧтобы начать работу - выберете что сделать"

#language=HTML
EXIT_MESSAGE = '<b>Главное меню</b>'

#language=HTML
NOTES_WORK = 'Начнем работу с <b>заметками</b>'

#language=HTML
ADD_NOTE = "<b>Чтобы добавить заметку делайте все по инструкции!</b>"

#language=HTML
NOTES = "Ваши <b>заметки</b>"

#language=HTML
NOTE_TITLE = "<b>Введите оглавление</b>"

#language=HTML
NOTE_TEXT = "<b>Введите текст</b>"

#language=HTML
NOTE_ADD_SUCCESS = "<b>Заметка добавлена!</b>"

#language=HTML
EVENTS_WORK = 'Начнем работу с <b>мероприятиями</b>'

#language=HTML
ADD_EVENT = "<b>Чтобы добавить мероприятие делайте все по инструкции!</b>"

#language=HTML
EVENTS = "Ваши <b>мероприятия</b>"

#language=HTML
EVENT_TITLE = "<b>Введите тему мероприятия</b>"

#language=HTML
EVENT_SUBSCRIPTION = "<b>Введите описание</b>"

#language=HTML
EVENT_DATE = "<b>Введите дату в формате ДД.ММ.ГГГГ</b>"

#language=HTML
EVENT_ADD_SUCCESS = "<b>Мероприятие добавлено!</b>"

#language=HTML
EVENT_DATE_INCORRECT = "<b>Дата введена не верно, давай еще раз</b>"
