import re
from typing import List


def get_str_lst(count: int = 0):
    str_lst = []
    for i in range(count):
        str_lst.append(input('Введите строку - '))
    return str_lst


def get_num_of_corr_str(str_lst: List[str] = list):
    pattern = re.compile('([0]*|[1]*|(10)*|(01)*)$')
    for i in range(len(str_lst)):
        if pattern.match(str_lst[i]) is not None:
            print(str(i + 1) + " - " + str_lst[i])


if __name__ == '__main__':
    lst_str = get_str_lst(count=int(input('Введите кол-во строк - ')))
    get_num_of_corr_str(lst_str)