from ClassWorks.HW7_help.rational_fraction import RationalFraction

if __name__ == '__main__':
    fraction_one = RationalFraction(4, 26)
    fraction_two = RationalFraction(1, 4)
    fraction_three = fraction_one + fraction_two
    print(fraction_three)
    print(fraction_one + fraction_two)
    print(fraction_one - fraction_two)
    print(fraction_one / fraction_two)