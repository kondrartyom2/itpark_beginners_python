class RationalFraction:
    def __init__(self, numerator: int = 0, denominator: int = 0):
        self.numerator = numerator
        self.denominator = denominator

    def reduce(self):
        limit = min(self.numerator, self.denominator)
        for divisor in reversed(range(1, limit + 1)):
            if self.numerator % divisor == 0 and self.denominator % divisor == 0:
                self.numerator, self.denominator = int(self.numerator / divisor), int(self.denominator / divisor)

    def __add__(self, fraction):
        answer = RationalFraction(
            int((self.numerator * fraction.denominator) + (fraction.numerator * self.denominator)),
            int(self.denominator * fraction.denominator)
        )
        answer.reduce()
        return answer

    def add(self, fraction):
        self.numerator = int((self.numerator * fraction.denominator) + (fraction.numerator * self.denominator))
        self.denominator = int(self.denominator * fraction.denominator)
        self.reduce()

    def __sub__(self, fraction):
        return RationalFraction(
            int((self.numerator * fraction.denominator) - (fraction.numerator * self.denominator)),
            int(self.denominator * fraction.denominator)
        ).reduce()

    def sub(self, fraction):
        self.numerator = int((self.numerator * fraction.denominator) - (fraction.numerator * self.denominator))
        self.denominator = int(self.denominator * fraction.denominator)
        self.reduce()

    def __mul__(self, fraction):
        return RationalFraction(
            int(self.numerator * fraction.numerator),
            int(self.denominator * fraction.denominator)
        ).reduce()

    def mult(self, fraction):
        self.numerator *= int(fraction.numerator)
        self.denominator *= int(fraction.denominator)
        self.reduce()

    def __truediv__(self, fraction):
        return RationalFraction(
            int(self.numerator * fraction.denominator),
            int(self.denominator * fraction.numerator)
        ).reduce()

    def div(self, fraction):
        self.numerator *= fraction.denominator
        self.denominator *= fraction.numerator
        self.reduce()

    def value(self) -> float:
        return self.numerator / self.denominator

    def __str__(self) -> str:
        return str(self.numerator) + '/' + str(self.denominator)