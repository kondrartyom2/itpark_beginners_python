from random import randrange
from typing import Type


class Player:

    def __init__(self, username: str):
        self.hp = 100
        self.username = username

    def __str__(self):
        return self.username + ': ' + str(self.hp)

    def is_alive(self):
        return True if self.hp > 0 else False

    def kicked(self, power: int):
        if 0 < power < 10:
            random = randrange(10)
            if power == 1:
                self.hp -= 10
            if power == 2 and random < 9:
                self.hp -= 20
            if power == 3 and random < 8:
                self.hp -= 30
            if power == 4 and random < 7:
                self.hp -= 40
            if power == 5 and random < 6:
                self.hp -= 50
            if power == 6 and random < 5:
                self.hp -= 60
            if power == 7 and random < 4:
                self.hp -= 70
            if power == 8 and random < 3:
                self.hp -= 80
            if power == 9 and random < 2:
                self.hp -= 90


def kick(player_one, player_two):
    if player_one.is_alive():
        print(str(player_one))
        player_two.kicked(int(input(player_one.username + " ты бьешь - вводи силу удара - ")))
        return False
    else:
        print("Победил игрок - {}".format(player_two.username))
        return True


if __name__ == '__main__':
    player_one = Player(input("Введите имя игрока 1 - "))
    player_two = Player(input("Введите имя игрока 2 - "))
    flag = True
    while True:
        if flag:
            if kick(player_one, player_two):
                break
            flag = False
        else:
            if kick(player_two, player_one):
                break
            flag = True