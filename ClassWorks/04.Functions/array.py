def get_array_by_user(count: int) -> list:
    array = []
    for i in range(count):
        array.append(int(input("Введите число - ")))
    return array


def add_array_by_user(count: int, array: list):
    for i in range(count):
        array.append(int(input("Введите число - ")))


def factorial(fact) -> int:
    if fact > 1:
        fact *= factorial(fact - 1)
    return fact


def get_factorials(lst: list) -> set:
    facts = set()
    for elem in lst:
        facts.add(factorial(elem))
    return facts


if __name__ == '__main__':
    #Хочу получать список
    lst = get_array_by_user(int(input('Введите кол-во элементов - ')))
    print(lst)
    add_array_by_user(int(input("Сколько еще элементов добавим - ")), lst)
    print(lst)
    facts_by_lst = get_factorials(lst)
    print(facts_by_lst)