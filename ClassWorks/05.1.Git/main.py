def bubble_sort(lst: list):
    length = len(lst)

    for i in range(length):
        for j in range(0, length - i - 1):
            if lst[j] > lst[j + 1]:
                lst[j], lst[j + 1] = lst[j + 1], lst[j]


def add_array_by_user(count: int, array: list):
    for i in range(count):
        array.append(int(input("Введите число - ")))


def get_array_by_user(count: int) -> list:
    array = []
    for i in range(count):
        array.append(int(input("Введите число - ")))
    return array


if __name__ == '__main__':
    lst = [2, 3, 1]
    bubble_sort(lst)