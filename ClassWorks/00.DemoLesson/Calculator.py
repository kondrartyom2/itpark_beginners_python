if __name__ == '__main__':
    while True:
        expression = input('Введите выражение: ')
        num_1 = int(expression.split(' ')[0])
        num_2 = int(expression.split(' ')[2])
        sign = expression.split(' ')[1]
        if sign == "+":
            print(num_1 + num_2, end=' - верно посчитал\nВсем спасибо всем пока')
        elif sign == "-":
            print(num_1 - num_2, end=' - верно посчитал\nВсем спасибо всем пока')
        elif sign == "*":
            print(num_1 * num_2, end=' - верно посчитал\nВсем спасибо всем пока')
        elif sign == "/":
            print(num_1 / num_2, end=' - верно посчитал\nВсем спасибо всем пока')