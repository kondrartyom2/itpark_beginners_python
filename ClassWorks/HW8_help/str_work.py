from pprint import pprint
from typing import List


def sort_str_print(first_str: str, second_str: str) -> str:
    min_length = min(len(first_str), len(second_str))
    for i in range(min_length):
        if first_str[i].lower() == second_str[i].lower():
            continue
        elif first_str[i].lower() < second_str[i].lower():
            return f'1. {first_str} \n2. {second_str}'
        else:
            return f'1. {second_str} \n2. {first_str}'
    if len(first_str) <= len(second_str):
        return '1. {}\n2. {}'.format(first_str, second_str)
    else:
        return '1. {}\n2. {}'.format(second_str, first_str)


def sort_str(first_str: str, second_str: str):
    min_length = min(len(first_str), len(second_str))
    for i in range(min_length):
        if first_str[i].lower() == second_str[i].lower():
            continue
        elif first_str[i].lower() < second_str[i].lower():
            return first_str, second_str
        else:
            return second_str, first_str
    if len(first_str) <= len(second_str):
        return first_str, second_str
    else:
        return second_str, first_str


def str_lst_sort(lst: List[str]):
    length = len(lst)

    for i in range(length):
        for j in range(0, length - i - 1):
            lst[j], lst[j + 1] = sort_str(lst[j + 1], lst[j])


def count_camel_case(string: str) -> int:
    count = 0
    for word in string.split(' '):
        if 'A' <= word[0] <= 'Z':
            count += 1
    return count


def count_of_letters(string: str) -> dict:
    answer = {}
    for i in range(len(string)):
        letter = string[i].lower()
        if 'a' <= letter <= 'z':
            if letter not in answer.keys():
                answer[letter] = 1
            else:
                answer[letter] += 1
    return answer


if __name__ == '__main__':
    print(sort_str_print(input('Первая строка - '), input('Вторая строка - ')))
    lst = ['Приветт', 'Привет', 'Андрей', 'Артем']
    str_lst_sort(lst)
    pprint(lst)
    print(count_camel_case(input('Введите строку - ')))
    pprint(count_of_letters(input('Введите строку - ')))
