class Vector:
    def __init__(self, x: float = 0.0, y: float = 0.0):
        self.x = x
        self.y = y

    def __str__(self) -> str:
        return '({}, {})'.format(self.x, self.y)

    def add(self, vector):
        self.x += vector.x
        self.y += vector.y

    def __add__(self, vector):
        return Vector(x=(self.x + vector.x), y=(self.y + vector.y))

    def sub(self, vector):
        self.x -= vector.x
        self.y -= vector.y

    def __sub__(self, vector):
        return Vector(x=(self.x - vector.x), y=(self.y - vector.y))

    def mult(self, vector):
        self.x *= vector.x
        self.y *= vector.y

    def __mul__(self, vector):
        return Vector(x=(self.x * vector.x), y=(self.y * vector.y))

    def __len__(self):
        return self.x + self.y

    def __eq__(self, vector):
        return True if self.x == vector.x and self.y == vector.y else False
